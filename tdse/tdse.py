import numpy as np
import scipy as sc
import matplotlib.pyplot as plt
from fgh import *
import matplotlib.animation as animation
''' Script for solving the time-dependent
Schrodinger equation (TDSE)

Uses finite differences to compute spatial
Laplacian and time-dependent integration
with Scipy's initial value problem (ivp_solve)
integrator

Supports arbitrary potentials and different
initial wave functions can be used as the
starting point. Wave packet and wfs for a custom
potential are supported. The custom potential
uses the Fourier grid  Hamiltonian (fgh) method to compute the wfs
in an arbitrary finite potential. Note that the step potential is not finite
and is unsuitable for fgh.

All the units are atomic units where hbar=1, energy and potential in
Hartree, x in Bohr
'''
# to make custom wfs compatible with the fgh method, the number of grid
#points needs to set rather than grid spacing
class TDSE:
        def __init__(self, xmin=-5, xmax=5, maxdx=0.02, npoints=2**10,
            t=10, plot_potential_and_initial_wf=True,
            potential={'type':'step', 'height':0.1,'center':0.},
            initial_wf={'type':'wavepacket','sigma':0.3, 'center':-2,'kx':0.1}):

            ''' Typical potentials:
            step
            wall, like a particle in a box, set wall_max and wall_min
            harmonic, 'type':'harmonic', 'center':0, 'frequency':1e13
            double well, 'type':dw, 'a':1, 'w':1
            custom, provide a numpy array as the potential
                'type':'custom', V=array
            barrier: type: barrier, position, width, height


            Typical initial_wf:
            wave packet: 'type:wavepacket','sigma':0.3, 'center':-2,'kx':0.1}
            custom: solved from the provided potential, only state is chosen,
                'state':0 is ground state, 1 1st excited...
                'mass':1 is proton,
            '''
            self.npoints = npoints
            self.x = np.linspace(xmin, xmax, self.npoints)
            self.dx = (np.max(self.x)-np.min(self.x)) / self.npoints
            assert self.dx < maxdx

            self.t = t
            if potential['type'] == 'step':
                self.V = self.step(potential)
            elif potential['type'] == 'box':
                self.V = self.box(potential)
            elif potential['type'] == 'barrier':
                self.V = self.barrier(potential)
            elif potential['type'] == 'harmonic':
                self.V = self.harmonic(potential)
            elif potential['type'] == 'dw':
                self.V = self.double_well(potential)
            elif potential['type'] == 'custom':
                self.V = potential['V']
            else:
                raise NameError('Incorrect potential type defined')

            if initial_wf['type'] == 'wavepacket':
                self.psi0 = self.wavepacket(initial_wf)
            elif initial_wf['type'] =='custom':
                self.psi0 = self.customwf(initial_wf)
            else:
                raise NameError('Incorrect initial wave function type defined')

            if plot_potential_and_initial_wf is True:
                plt.plot(self.x, self.V,label='potential')
                plt.plot(self.x, np.abs(self.psi0)**2, label='$\Psi_0^2$')
                plt.xlabel('x [Bohr]')
                plt.ylabel('E [Ha]')
                plt.legend()
                plt.show()

        def step(self, potential):
            V = np.zeros(self.x.shape)
            center = potential['center']
            height = potential['height']
            for i, _x in enumerate(self.x):
                if _x >= center:
                    V[i] += height
            return V

        def barrier(self, potential):
            V = np.zeros(self.x.shape)
            position = potential['position']
            width = potential['width']
            height = potential['height']
            for i, _x in enumerate(self.x):
                if _x >= position-width/2. and _x <= position+width/2.:
                    V[i] += height
            return V

        def harmonic(self, potential):
            # note! Here frequency is in hertz
            # convert to atomic units
            T= potential['period']
            center = potential['center']
            omega = 2 * np.pi / T
            k = omega**2
            V = 0.5 * k * (self.x - center)**2
            return V

        def box(self, potential):
            V = np.zeros(self.x.shape)
            wall_min = potential['wall_min']
            wall_max = potential['wall_max']
            height = potential['height']
            for i, _x in enumerate(self.x):
                if _x >= wall_max or _x <= wall_min:
                    V[i] += height
            return V

        def double_well(self, potential):
            # symmetric double well with V(x)=a*(x-w)^2*(x+w)^2
            # centered at x=0
            # a controls height, w the width
            a = potential['a']
            w = potential['w']
            return a*(self.x-w)**2*(self.x+w)**2

        def wavepacket(self, initial_wf):
            # x=grid sigma=width, x0= wp center, kx=momentum in x
            sigma = initial_wf['sigma']
            center = initial_wf['center']
            kx = initial_wf['kx']
            A = 1.0 / (sigma * np.sqrt(np.pi)) # normalization constant
            # Initial Wavefunction
            psi0 = np.sqrt(A) * np.exp(-(self.x-center)**2 / (2.0 * sigma**2)) * np.exp(1j * kx * self.x)
            return psi0

        def customwf(self, initial_wf):
            # V=potential, x=grid,
            # mass=particle mass --> 1 is proton
            # state=which solution, 0 is ground state, 1=1st excited...
            ham = fgh(self.V, max(self.x), initial_wf['mass'],
                convert_to_atomic_units=False)
            psi0 = ham.get_wavefunction(index=initial_wf['state'])
            return psi0

        def get_laplacian(self):
            #builds the Laplacian using central finite difference
            #As the laplacian is tridiagonal, this is done using
            #scipy sparse linear algebra
            lapl = sc.sparse.diags([1, -2, 1], [-1, 0, 1],
                    shape=(self.x.size, self.x.size)) / self.dx**2
            return lapl

        def get_rhs(self, t, psi):
            #return the right hand side of
            #-i*d/dt Psi = -[-1/2*d^2/dx^2+V(x)] Psi
            #[0.5*d^2/dx^2+V(x)] is the Hamiltonian H
            # operate psi with laplacian, add potential
            print('t%5.4f'%t)
            return -1.j * (-0.5 * self.lapl.dot(psi) + self.V*psi)

        def propagate(self, method='RK23', dt=0.1):
            self.lapl = self.get_laplacian()
            self.get_rhs(0, self.psi0)
            #method = numerical integration, choose from scipy solve_ivp
            #dt = time interval for saving
            self.sol = sc.integrate.solve_ivp(self.get_rhs,
                          t_span=[0, self.t],
                          y0=self.psi0,
                          t_eval=np.arange(0, self.t, dt),
                          method=method)
            return self.sol

        def show_images(self, colormap='rocket'):
            # image_indices list of indices
            fig, axs = plt.subplots(3, 1, sharex = 'col')

            for i, t in enumerate(self.sol.t):
                axs[0].plot(self.x, np.real(self.sol.y[:,i]), label='t=%3.2f'%t)
                axs[1].plot(self.x, np.imag(self.sol.y[:,i]),label='t=%3.2f'%t)
                axs[2].plot(self.x, np.abs(self.sol.y[:,i])**2,label='t=%3.2f'%t)
            axs[0].set_ylabel('Real part of $\Psi$')
            axs[1].set_ylabel('Imaginary part of $\Psi$')
            axs[2].set_ylabel('$\Psi^2$')
            axs[2].set_xlabel('x [Bohr]')
            plt.show()

        def test_laplace_on_sin(self):
            lapl = self.get_laplacian()
            sin = np.sin(self.x)
            d2dx2 = lapl.dot(np.sin(self.x))
            plt.plot(self.x, sin, label='sin')
            plt.plot(self.x, d2dx2, label='laplacian of sin')
            plt.legend()
            plt.show()

        def test_laplace_on_wf(self):
            lapl = self.get_laplacian()
            d2dx2 = lapl.dot(self.psi0)
            plt.plot(self.x, self.psi0, label='wf')
            plt.plot(self.x, d2dx2, label='laplacian of wf')
            plt.legend()
            plt.show()

        def normalization(self):
            for i, t in enumerate(self.sol.t):
                psi2 = np.trapz(np.abs(self.sol.y[:, i])**2, self.x)
                print('time:%3.2f, int(|psi|^2):%7.6f' % (t, psi2))

        def make_video_separate(self, type='probability', filename='tdse',
            scale_potential=0.1, show=True, save=False, ymax=False, ymin=False):
            # type : real, imag, or probability
            # initializing a figure

            if type == 'probability':
                data = np.abs(self.sol.y)**2
                legend = '$\mid \Psi \mid^2$'
            elif type == 'real':
                data = np.real(self.sol.y)
                legend = '$\Psi$'
            elif type == 'imag':
                data = np.imag(self.sol.y)
                legend = '$\Psi^*$'
            else:
                raise NameError('Incorrect initial data type defined')

            fig = plt.figure()
            # labeling the x-axis and y-axis
            if ymin or ymax is False:
                ymin = np.min(data)-0.5
                ymax = np.max(data)
            axis = plt.axes(xlim=(np.min(self.x), np.max(self.x)),
                    ylim=(ymin, ymax))
            time_text = axis.text(0.4, 0.90, '', transform=axis.transAxes)

            # initializing a line variables and legends
            line1, = axis.plot([], [], lw=3)
            line2, = axis.plot([], [], lw=1)
            def animate(frame_number):
                # plots a sine graph
                line1.set_data(self.x, data[:,frame_number])
                line1.set_color('green')
                line1.set_label(legend)
                line2.set_data(self.x, scale_potential*self.V)
                line2.set_color('blue')
                line2.set_label('Potential')
                axis.set_xlabel('x [Bohr]')
                axis.legend(loc=1, prop={'size': 12})
                time_text.set_text('t = %4.3f [au]' % self.sol.t[frame_number])
                return line1, line2, time_text

            anim = animation.FuncAnimation(fig, animate, frames=len(self.sol.t),
                               interval=200)
            if show:
                plt.show()
            if save:
                anim.save(filename+'.gif', writer='imagemagick', fps=10)

        def make_video_imag_and_real(self, filename='tdse',
            scale_potential=0.1, show=True, save=False, ymax=False, ymin=False):

            fig = plt.figure()
            axis = plt.axes(xlim=(np.min(self.x), np.max(self.x)),
            ylim=(ymin, ymax))

            # initializing a line variable
            line1, = axis.plot([], [], lw=2)
            line2, = axis.plot([], [], lw=1)
            line3, = axis.plot([], [], lw=2)

            def animate(frame_number):
                line1.set_data(self.x, np.real(self.sol.y[:,frame_number]))
                line1.set_color('green')
                line1.set_label('$\Psi$')
                line2.set_data(self.x, scale_potential*self.V)
                line2.set_color('blue')
                line2.set_label('Potential')
                axis.set_xlabel('x [Bohr]')
                axis.legend(loc=1, prop={'size': 12})
                line3.set_data(self.x, np.imag(self.sol.y[:,frame_number]))
                line3.set_color('black')
                line3.set_label('$\Psi^*$')
                return line1, line2, line3
            anim = animation.FuncAnimation(fig, animate, frames=len(self.sol.t),
                               interval=200)
            if show:
                plt.show()
            if save:
                anim.save(filename+'.gif', writer='imagemagick', fps=10)
