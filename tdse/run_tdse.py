from tdse import *

#start
tdse = TDSE(xmin=-10, xmax=10, maxdx=0.005, npoints=2**12,
    t=1.5, plot_potential_and_initial_wf=False,
    #potential={'type':'box', 'wall_min':-3, 'wall_max':3,'height':10000},
    #initial_wf={'type':'wavepacket','sigma':0.5, 'center':0,'kx':10})
    #potential={'type':'dw', 'a':1., 'w':1.},
    #initial_wf={'type':'wavepacket','sigma':0.3, 'center':-1.,'kx':0.}
    #potential={'type':'step', 'center':0, 'wall_max':4,'height':50},
    #initial_wf={'type':'wavepacket','sigma':0.5, 'center':-6,'kx':10}
    potential={'type':'barrier', 'position':0, 'width':0.1,'height':150},
    initial_wf={'type':'wavepacket','sigma':0.5, 'center':-6,'kx':10}
    )
#propagate in time
sol = tdse.propagate(dt=5e-2)
#tdse.show_images()
tdse.make_video_separate(type='probability', save=True, filename='barrier_psi2_tdse',
                        scale_potential=0.1)
tdse.make_video_imag_and_real(ymin=-1, ymax=1, scale_potential=0.1,save=True,
filename='barrier_imag_real_tdse')
#tdse.test_laplace_on_wf()
tdse.normalization()
