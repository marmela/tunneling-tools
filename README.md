This repository contains Python implementations for
 - two ways of solving the 1D time-independent Schrödinger equation (TISE) with arbitrary potentials. Also tunnel splittings are computed
 - solving the 1D time-dependent Schrödinger equation (TDSE) with several
 different initial state and potential options. Also more refined visualization options to make gifs
