from fgh import *
import numpy as np
import matplotlib.pyplot as plt

# asymmetric double well
delta = 10
Vmax = 50
b = 2
a = 2

#symmetric double wall
delta = 0
Vmax = 0.3
b = 0.3
a = 0.

#V =lambda x: 5000*x**4 - 3162*x**2 + 500

V = lambda x: (Vmax/b**4)*((x-a/2)**2-b**2)**2-delta* x
#V = lambda x: 50*x**2
#V = lambda x: (Vmax/b**2)*(np.abs(x-a/2)-b)**2 -delta* x

def potential(x):
	#F. Fillaux, B. Nicolaı / Chemical Physics Letters 415 (2005) 357–361
	return -57.654*x**6 + 0.7924*x**5 + 67.774*x**4 - 0.3934*x**3 - 11.483*x**2 + 0.0309*x + 0.5277

N = 2**5+1
L = 2.5
x_vals = np.linspace(-L/2,L/2, N, endpoint=True)
#V_sampled = potential(x_vals)
V_sampled = V(x_vals)
plt.plot(x_vals,V_sampled)
plt.show()


ham = fgh(V_sampled,L, mass=2., colorpalette='pastel')
ham.plot_all(psi_max=4, ylim=[0,1], xlim=[-0.75,0.75])
