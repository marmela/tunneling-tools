from itertools import cycle

import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eigh
from scipy.integrate import simps
from math import sin
from scipy import interpolate
import seaborn as sns

''' The Fourier grid Hamiltonian method

Implemented based on the book
David Tannor
Introduction to Quantum Mechanics: A Time-Dependent Perspective
University Science Books, 2007''' 

class fgh:
    def __init__(self, Vq, Q, mass = 1, npoints=2**10, interpolate = True,
                interpolation_type = 'cubic', colorpalette=None):
        '''
           Vq: vector with potential energy surface along q direction
               unit: eV

           NOTE! Vq needs to have UNEVEN amount of points!!!!!

           Q distance (in angstrom) of the interval --> Vq in [0,Q]
           Q is translated so that Vq is sampled [-Q/2,Q/2]
           mass = mass # in Daltons/atomic mass units --> m = 1 is a proton


           <q|H|q'> = <q|T(p)|q'> + <q|V(q)|q'>
           V_qq' =  V(q) delta(q,q') --> diagonal
           T_qq' = K^2/3(1+2/N^2) (q=q')
               2K^2/N^2 (-1)^(q-q')/sin(pi*(q-q')/N)
           K = Q/npoints
           colorpalette = seaborn colorpatte'''
        self.Ha2eV = 27.211385
        self.B2A = 0.52917721092
        self.Vq = Vq / self.Ha2eV  # energy in atomic units
        self.Q = Q / self.B2A  # distance in atomic units
        self.mass = mass * 1837.  # mass in atomic units, m_electron = 1
        self.interpolate = interpolate
        N = int(len(self.Vq))
        self.q = np.linspace(-self.Q/2, self.Q/2, N, endpoint=False)  # path
        self.npoints = npoints  # how many points for interpolation
        self.interpolation_type = interpolation_type

        if colorpalette is not None:
            sns.set_palette(colorpalette)

    def diagonalize(self):
        if not hasattr(self, 'Hij'):
            self.make_hamiltonian()
        E, psi = np.linalg.eigh(self.Hij)
        print(E)
        return E, psi

    def plot_all(self, psi_min=0, psi_max=10, ylim=None, xlim = None):
        '''psi_min and max: how many wave functions to plot '''

        E, psi = self.diagonalize()
        E *= self.Ha2eV
        fig, (ax0, ax1, ax2) = plt.subplots(nrows=3)
        ax2.plot(self.q*self.B2A, self.Vq*self.Ha2eV, ls="-", c="k", lw=2, label="$Vq$")

        #plt.rc('axes', color_cycle=["blue", "red", "gray", "orange", "darkturquoise", "magenta"])

        style_cycler = cycle(["-", "--"])  # line styles for plotting
        color_cycle = ["blue", "red", "gray", "orange", "darkturquoise", "magenta"]

        for i in range(psi_min, psi_max):
            psi = self.get_wavefunction(index=i)
            # WF
            ax0.plot(self.q*self.B2A, psi + i, lw=1.5, label="$\psi_%s$" % i)
            # density
            ax1.plot(self.q*self.B2A, (psi**2))
            # Vibrational energies
            ax2.plot(self.q*self.B2A, E[i]*np.ones(len(self.q)), ls='--', lw=1.5)

        #ax0.set_xlabel('Q [$\AA$]')
        ax0.set_ylabel(r'Aaltofunktio [$\Psi$]')
        #ax1.set_xlabel('Q [$\AA$]')
        ax1.set_ylabel(r'Tiheys [$\rho=\Psi^2$]')

        ax2.set_xlabel(r'Protonikoordinaatti [$\AA$]')
        ax2.set_ylabel(r'E [eV]')
        if ylim is not None:
            ax2.set_ylim(ylim[0],ylim[1])
        if xlim is not None:
            ax0.set_xlim(xlim[0],xlim[1])
            ax1.set_xlim(xlim[0],xlim[1])
            ax2.set_xlim(xlim[0],xlim[1])
        # plt.legend(loc="best")
        split = (E[1]-E[0])
        plt.tight_layout()
        print('Tunnel splitting: %4.3f eV' % (split))
        plt.show()

    def plot_potential_and_vibrations(self, Qmin=False, Qmax=False, Vmin=False, Vmax=False,
                                      plot_vibs=False, psi_min=0, psi_max=4,save=False, colors=['orange','blue','magenta','black']):

        fig, (ax0) = plt.subplots(nrows=1)
        ax0.plot(self.q*self.B2A, self.Vq*self.Ha2eV, ls="-", c="k", lw=2, label="$Vq$")
        if Qmin and Qmax:
            ax0.set_xlim(Qmin,Qmax)
        if Qmin and Qmax:
            ax0.set_ylim(Vmin,Vmax)
        if plot_vibs:
            E, psi = self.diagonalize()
            E *= self.Ha2eV
            for i in range(psi_min, psi_max):
                ax0.plot(self.q*self.B2A, E[i]*np.ones(len(self.q)), ls='--', lw=1.5,c=colors[i])

        ax0.set_xlabel('H-coordinate [$\AA$]', fontsize=16)
        ax0.set_ylabel('E [eV]', fontsize=16)
        plt.tight_layout()
        if save:
            plt.savefig('pot_and_vibs.png', dpi=600)
        plt.show()

    def plot_wf_and_dens(self, Qmin=False, Qmax=False, psi_min=0, psi_max=4,
        save=False,colors=['orange','blue','magenta','black']):
        fig, (ax0,ax1) = plt.subplots(nrows=2)
        E, psi = self.diagonalize()
        E *= self.Ha2eV

        for i in range(psi_min, psi_max):
            psi = self.get_wavefunction(index=i)
            # WF
            ax0.plot(self.q*self.B2A, psi + i, lw=1.5, label="$\psi_%s$" % i,c=colors[i])
            ax1.plot(self.q*self.B2A, (psi**2),c=colors[i])
        if Qmin and Qmax:
            ax0.set_xlim(Qmin,Qmax)
            ax1.set_xlim(Qmin,Qmax)

        ax1.set_xlabel('H-coordinate [$\AA$]', fontsize=16)
        ax0.set_ylabel(r'$\Psi$', fontsize=16)
        ax1.set_ylabel(r'$\rho$', fontsize=16)
        plt.tight_layout()
        if save:
            plt.savefig('wf_and_dens.png', dpi=600)
        plt.show()

    def interpolate_path(self, plot_path=False):
        '''From given energies Vq(q) at reaction coordinate (q)
        interpolate a smooth reaction path E(q) to be used in the Ie
        integration (action integral)'''

        E = interpolate.interp1d(self.q, self.Vq, kind=self.interpolation_type)

        qq = np.linspace(self.q[0], self.q[-1], self.npoints, endpoint=True)
        Vqq = E(qq)

        if plot_path:
            plt.plot(self.q, self.Vq, 'ro', label='Original')
            plt.plot(qq, Vqq, ls="-", c="k", label='interpolated')
            plt.xlabel('Qp [Bohr]')
            plt.ylabel('E [Ha]')
            plt.show()

        return qq, Vqq

    def make_hamiltonian(self):
        if self.interpolate:
            self.q, self.Vq = self.interpolate_path()
        N = len(self.q)
        assert(N % 2 == 0)
        self.Hij = np.zeros((N, N))

        self.K = np.pi / (self.Q / N)  # spacing of points

        for i in range(N):
            for j in range(i+1):
                if i == j:
                    self.Hij[i, j] += (1./(6*self.mass)*self.K**2)/(1. + 2./N**2) + self.Vq[i]
                else:
                    self.Hij[i, j] += ((1./(self.mass)) * (self.K/N)**2.) * ((-1.)**(j-i)) / (np.sin(np.pi * (j-i)/N))**2.
                    self.Hij[j, i] += np.conj(self.Hij[i,j])

    def get_wavefunction(self, index=0):
        if not hasattr(self, 'Hij'):
            self.make_hamiltonian()
        E, eigenvectors = self.diagonalize()
        psi = eigenvectors[:, index]
        psi /= np.sqrt(np.trapz(np.abs(psi)**2, self.q))
        return psi

