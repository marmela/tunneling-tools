from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from itertools import cycle
import itertools
class DoubleWell:
    def __init__(self, Vq, q, tunneling_species='H', expansion_order = 50,
               interpolation_type = 'cubic', npoints = 1000):

        ''' Method from The double-well potential in quantum
        mechanics: a simple, numerically exact
        formulation

        Vq: potential energy surface (PES) on grid, unit eV,
            the 0 and L points need to be repulsive

        q: reaction coordinate grid in angstrom i.e. E = Vq(q),
           going from 0 to L '''

        self.Vq = Vq / 27.211385 # in a.u.
        self.q = q / 0.52917721092 # in a.u.
        self.q -= self.q[0]
        self.L = self.q[-1]
        #if tunneling_species == 'e':
        #    self.m = 1 #electron
        #else:
        #    self.m = atomic_masses[tunneling_species]*1837
        self.m = 1
        self.interpolation_type = interpolation_type # degree of spline interpolation
        self.npoints = npoints # how many points to use in interpolated function
        self.expansion = expansion_order

    def interpolate_path(self, plot_path = True):
        '''From given energies Vq(q) at reaction coordinate (q)
        interpolate a smooth reaction path E(q) to be used in the Ie
        integration (action integral)'''

        E = interpolate.interp1d(self.q, self.Vq, kind = self.interpolation_type)
        self.qq = np.linspace(self.q[0], self.q[-1], self.npoints, endpoint=True)
        self.Vqq = E(self.qq)

        if plot_path:
            plt.plot(self.q, self.Vq,'ro', label = 'Original')
            plt.plot(self.qq,self.Vqq,ls="-", c="k", label = 'interpolated')
            plt.xlabel('Qp [Bohr]')
            plt.ylabel('E [Ha]')
            plt.show()
        return self.qq, self.Vqq

    def box_energies(self):
         self.Ebox = np.zeros(self.expansion)
         E1 = np.pi**2/(2*self.m*self.L**2)
         for n in range(self.expansion):
             self.Ebox[n] = (n+1)**2 * E1

    def make_hamiltonian_matrix(self):
         if not hasattr(self,'Ebox'):
             self.box_energies()

         self.interpolate_path(plot_path = False)
         self.basis_generator()

         self.Hnm = np.zeros((self.expansion,self.expansion))
         for n in range(0,self.expansion):
             self.Hnm[n,n] += self.Ebox[n]
             for m in range(n,self.expansion):

                 self.Hnm[n,m] += 2./self.L *np.trapz(self.basis[n]*\
                             self.Vqq *self.basis[m], self.qq)
                 self.Hnm[m,n] = self.Hnm[n,m]

    def basis_generator(self):
        basis = []
        for n in range(self.expansion):
            basis.append(np.sin((n+1)*np.pi*self.qq/self.L))
        self.basis = np.asarray(basis)


    def diagonalize(self):
        self.make_hamiltonian_matrix()
        E, eigenvectors = np.linalg.eigh(self.Hnm)
        return E, eigenvectors

    def plot(self, psi_min = 0, psi_max = 6):
        '''psi_min and max: how many wave functions to plot '''

        E, eigenvectors = self.diagonalize()
        plt.plot(self.q, self.Vq,'ro', label="$Vq$")
        plt.plot(self.qq,self.Vqq,ls="-", c="k", lw=2, label="$Vqq$" )
        style_cycler = cycle(["-", "--"])  # line styles for plotting
        color_cyler = cycle(["blue", "red", "gray", "orange", "darkturquoise", "magenta"])

        for i in range(psi_min,psi_max):
            psi = np.dot(eigenvectors[:,i],self.basis)
            norm = np.trapz(np.abs(psi)**2, x = self.qq)
            psi /= np.sqrt(norm)
            plt.plot(self.qq, psi + E[i] ,ls=next(style_cycler), lw=1.5,
                color=next(color_cyler), label="$\psi_%s$"%i)

        plt.xlabel('Q [Bohr]')
        plt.ylabel('E [Ha]')
        #plt.legend(loc="best")
        plt.show()

    def get_wavefunction(self, index=0):
        if not hasattr(self,'Hnm'):
            self.make_hamiltonian_matrix()
        E, eigenvectors = self.diagonalize()
        psi = np.dot(eigenvectors[:,index],self.basis)
        psi /= np.sqrt(np.trapz(np.abs(psi)**2,self.qq))
        return psi
