from MatrixMechanics_DoubleWell import *
import numpy as np


# asymmetric double well
#delta = 100
#Vmax = 200
#b = 1
#a = 1

#symmetric double wall
delta = -5
Vmax = 50
b = 1
a = 1

#V = lambda x: (Vmax/b**4)*((x-a/2)**2-b**2)**2-delta* x 
#V = lambda x: 10*x**2
V = lambda x: (Vmax/b**2)*(np.abs(x-a/2)-b)**2 -delta* x  

N = 2**5 +1 # number of samples in discretization of potential
L = 10.  # length of x support
x_vals = np.linspace(-L/2,L/2, N, endpoint=True)
V_sampled = V(x_vals)

ham = DoubleWell(V_sampled, x_vals)
#ham.interpolate_path()
ham.plot()
